"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""

from bs4     import BeautifulSoup
from decimal import Decimal

from .decorators import safe_decimal, safe_integer, safe_string
from django.conf import settings
from django.template.defaultfilters             import slugify
from webscrapper.tools.templates.BaseTemplate   import BaseTemplate

import os
import requests


class MercadoLibreTemplate(BaseTemplate):

    def __init__(self):
        self.links          = []
        self.itemslinks     = []
        self.pages          = 20
        self.items_per_page = 48
        self.the_soup       = None

    """
    Template methods =================
    """
    def SetSoup(self, the_soup):
        self.the_soup = the_soup

    @safe_string
    def GetTitle(self):
        data = self.the_soup.findAll('h1', attrs={'class' : 'item-title__primary'})
        return data[0].getText().strip()

    @safe_decimal
    def GetPrice(self):
        data = self.the_soup.findAll('span', attrs={'class' : 'price-tag-fraction'})
        data = data[0].getText().strip().replace(',','')
        return Decimal(data)

    @safe_string
    def GetAddress(self):
        data_1 = self.the_soup.findAll('h2', attrs={'class' : 'map-address'})
        data_2 = self.the_soup.findAll('h3', attrs={'class' : 'map-location'})
        return '{0}, {1}'.format(data_1[0].getText().strip(), data_2[0].getText().strip())

    @safe_decimal
    def GetBaths(self):
        data  = self.the_soup.findAll('li', attrs={'class' : 'specs-item'})
        value = None

        for item in data:
            title = item.findAll('strong')
            if len(title) > 0:
                t = slugify(title[0].getText().strip())
                if t == 'banos':
                    value = item.findAll('span')
                    value = value[0].getText().strip()
                    break

        value = value.split(' ')
        return int(value[len(value)-1])

    @safe_integer
    def GetRooms(self):
        data  = self.the_soup.findAll('li', attrs={'class' : 'specs-item'})
        value = None

        for item in data:
            title = item.findAll('strong')
            if len(title) > 0:
                t = slugify(title[0].getText().strip())
                if t == 'recamaras':
                    value = item.findAll('span')
                    value = value[0].getText().strip()
                    break

        value = value.split(' ')
        return int(value[len(value)-1])

    @safe_string
    def GetState(self):
        data = self.the_soup.findAll('h3', attrs={'class' : 'map-location'})
        data = data[0].getText().strip().split(',')
        return data[len(data)-1]

    @safe_string
    def GetColony(self):
        data = self.the_soup.findAll('h3', attrs={'class' : 'map-location'})
        data = data[0].getText().strip().split(',')
        return data[0]

    @safe_string
    def GetTown(self):
        data  = self.the_soup.findAll('a', attrs={'class' : 'breadcrumb'})
        return data[len(data)-2].getText().strip()

    @safe_string
    def GetDescription(self):
        data  = self.the_soup.findAll('div', attrs={'class' : 'item-description__text'})
        return str(data[0])

    @safe_string
    def GetPropertyType(self):
        data  = self.the_soup.findAll('article', attrs={'class' : 'vip-classified-info'})
        data  = data[0].findAll('dl')
        data  = data[0].getText().strip().split(' ')
        return data[0]

    @safe_string
    def GetOperationType(self):
        data  = self.the_soup.findAll('a', attrs={'class' : 'breadcrumb'})
        return data[2].getText().strip()

    def GetPhotos(self, property):
        path       = '{0}{1}'.format(settings.STATIC_PATH_PHOTOS, property.hashedname)
        imageslist = []
        #already exists
        if os.path.isdir(path):
            return False

        path_large = '{0}/large/'.format(path)
        path_front = '{0}/front/'.format(path)
        path_thumb = '{0}/thumb/'.format(path)

        os.makedirs(path)
        os.makedirs(path_front)
        os.makedirs(path_large)
        os.makedirs(path_thumb)

        #thumbnails
        items = self.the_soup.findAll('label', attrs={ 'class' : 'gallery__thumbnail' })
        for label in items:
            images = label.findAll('img')
            if len(images) > 0:
                image = images[0]
                src   = image.get('src')
                name  = self.__NormalizeName(src)

                f = open(path_thumb + name,'wb')
                f.write(requests.get(src).content)
                f.close()

        #front & large
        items = self.the_soup.findAll('a', attrs={ 'class' : 'gallery-trigger' })
        for link in items:
            src   = link.get('href')
            name  = self.__NormalizeName(src)

            imageslist.append(name)

            f = open(path_large + name,'wb')
            f.write(requests.get(src).content)
            f.close()

            images = link.findAll('img')
            if len(images) > 0:
                src   = images[0].get('src')
                name  = self.__NormalizeName(src)

                f = open(path_front + name,'wb')
                f.write(requests.get(src).content)
                f.close()

        property.SetPhotos(imageslist)

    def BuildLinks(self):
        base_url = 'https://inmuebles.mercadolibre.com.mx/distrito-federal/'
        self.links.append(base_url)
        for i in range(1, self.pages):
            since = (i *self.items_per_page) + 1
            self.links.append('{0}_Desde_{1}'.format(base_url, since))

        self.__GetItemLinks()

        return self.itemslinks

    """
    Private methods =================
    """
    def __NormalizeName(self, src):
        name = src.split('/')[-1]
        ext  = name.split('.')[-1]
        data = name.split('-')
        MLM  = [i for i in data if i.startswith('MLM')]

        fname = name
        if len(MLM) > 0:
            fname = '{0}.{1}'.format(MLM[0].split('_')[0], ext)

        return fname

    def __GetItemLinks(self):
        for link in self.links:
            req = requests.get(link)
            soup = BeautifulSoup(req.text, "lxml")
            links = soup.findAll('a', attrs={'class': 'item-link' })
            for item in links:
                self.itemslinks.append(item.get('href'))



