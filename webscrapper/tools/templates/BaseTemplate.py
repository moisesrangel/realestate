"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""
from abc import ABC, ABCMeta, abstractmethod

class BaseTemplate(ABC):
    __metaclass__ = ABCMeta

    @abstractmethod
    def SetSoup(self):
        pass

    @abstractmethod
    def BuildLinks(self):
        pass

    @abstractmethod
    def GetTitle(self):
        pass

    @abstractmethod
    def GetPrice(self):
        pass

    @abstractmethod
    def GetAddress(self):
        pass

    @abstractmethod
    def GetBaths(self):
        pass

    @abstractmethod
    def GetRooms(self):
        pass

    @abstractmethod
    def GetState(self):
        pass

    @abstractmethod
    def GetColony(self):
        pass

    @abstractmethod
    def GetTown(self):
        pass

    @abstractmethod
    def GetDescription(self):
        pass

    @abstractmethod
    def GetPropertyType(self):
        pass

    @abstractmethod
    def GetOperationType(self):
        pass

    @abstractmethod
    def GetPhotos(self, property):
        pass


