"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Development"

"""

from decimal import Decimal

def safe_string(f):
    def wrapper(*args):
        try:
            result = f(*args)
        except Exception as e:
            print(e)
            result = '-'
        return result
    return wrapper

def safe_integer(f):
    def wrapper(*args):
        try:
            result = f(*args)
        except Exception as e:
            print(e)
            result = 0
        return result
    return wrapper

def safe_decimal(f):
    def wrapper(*args):
        try:
            result = f(*args)
        except Exception as e:
            print(e)
            result = Decimal('0')
        return result
    return wrapper