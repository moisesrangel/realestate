"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""

from webscrapper.models import Property, Version
from bs4 				import BeautifulSoup

import django.dispatch
import requests
import zope.event

class PropertyCreatedEvent(object):
	def __init__(self, property):
		self.property = property

class scraper(object):

	def __init__(self, template):
		self.template = template
		self.version  = None

	def Execute(self, sourcename):
		self.__GetVersion()
		links = self.template.BuildLinks()

		for link in links:

			print(link)
			req = requests.get(link)
			soup = BeautifulSoup(req.text, "lxml")

			self.template.SetSoup(soup)

			prop = Property()

			prop.address 	   = self.template.GetAddress()
			prop.baths   	   = self.template.GetBaths()
			prop.colony		   = self.template.GetColony()
			prop.description   = self.template.GetDescription()
			prop.operationtype = self.template.GetOperationType()
			prop.price   	   = self.template.GetPrice()
			prop.propertytype  = self.template.GetPropertyType()
			prop.rooms         = self.template.GetRooms()
			prop.state		   = self.template.GetState()
			prop.title   	   = self.template.GetTitle()
			prop.town		   = self.template.GetTown()

			prop.source        = sourcename
			prop.url     	   = link
			prop.version       = self.version

			prop.save()

			self.template.GetPhotos(prop)

			event = PropertyCreatedEvent(prop)
			zope.event.notify(event)

	def __GetVersion(self):
		items	= Version.objects.all().order_by('-created_date')
		if len(items) == 0:
			raise Exception("You must generate a version before scraping. Run the command 'generate-scraping-version' ")

		self.version = items[0]
