"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Development"

"""

from django.db                       import models
from django.db.models.signals        import pre_save
from django.dispatch                 import receiver
from django.template.defaultfilters  import slugify

import hashlib

class BaseModel(models.Model):
    created_date  = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Version(BaseModel):
    number = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return str(self.number)

class Photo(BaseModel):
    name      = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Property(BaseModel):

    SOURCES = [
        ('MercadoLibre', 'MercadoLibre'),
        ('Segundamano','Segundamano'),
        ('MetrosCubicos', 'MetrosCubicos'),
        ('Vivanuncios', 'Vivanuncios'),
    ]

    TYPES = [
        ('Casa', 'Casa'),
        ('Departamento' ,'Departamento'),
        ('Desarrollo' ,'Desarrollo'),
        ('Terreno','Terreno'),
        ('Local','Local'),
    ]

    OPTYPES = [
        ('Renta', 'Renta'),
        ('Venta' ,'Venta'),
    ]

    address         = models.CharField(max_length=200)
    baths           = models.DecimalField(max_digits=3, decimal_places=1)
    colony          = models.CharField(max_length=200)
    colony_slug     = models.CharField(max_length=200)
    delivery_date   = models.DateField(null=True, blank=True)
    description     = models.TextField()
    hashedname      = models.CharField(max_length=200)
    operationtype   = models.CharField(max_length=50, choices=OPTYPES)
    photos          = models.ManyToManyField(Photo)
    price           = models.DecimalField(max_digits=10, decimal_places=2)
    propertytype    = models.CharField(max_length=50, choices=TYPES)
    rooms           = models.IntegerField()
    source          = models.CharField(max_length=50, choices=SOURCES)
    state           = models.CharField(max_length=200)
    state_slug      = models.CharField(max_length=200)
    title           = models.CharField(max_length=200)
    town            = models.CharField(max_length=200)
    town_slug       = models.CharField(max_length=200)
    url             = models.URLField(max_length=200)
    version         = models.ForeignKey(Version, on_delete=models.CASCADE)

    def __str__(self):
        return "{0} ({1})".format(self.title, self.address)

    def SetPhotos(self, ilist):
        photos = [(lambda x: Photo.objects.create(name=x) )(x) for x in ilist]
        [(lambda x: self.photos.add(x) )(x) for x in photos]

@receiver(pre_save, sender=Version)
def version_callback(sender, instance, *args, **kwargs):
    pass

@receiver(pre_save, sender=Property)
def property_callback(sender, instance, *args, **kwargs):
    instance.state_slug   = slugify(instance.state)
    instance.town_slug    = slugify(instance.town)
    instance.colony_slug  = slugify(instance.colony)
    instance.hashedname   = hashlib.sha224(instance.url.encode('utf-8')).hexdigest()
