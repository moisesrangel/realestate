"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""

from django.core.management.base   import BaseCommand, CommandError
from django.dispatch               import receiver
from webscrapper.tools.scraper     import scraper

import importlib
import zope.event

class Command(BaseCommand):
    help = 'Scraping properties from selected source'

    def add_arguments(self, parser):
        parser.add_argument('source', nargs='+', type=str)

    def handle(self, *args, **options):

        zope.event.subscribers.append(lambda x : self.stdout.write(self.style.SUCCESS('Successfully created property "%s"' % x.property.title) ))

        sourcename  = options['source'][0]
        path        = 'webscrapper.tools.templates.{0}'.format(sourcename.lower())
        module      = importlib.import_module(path)
        base        = getattr(module, '{0}Template'.format(sourcename))
        template    = base()
        scrap       = scraper(template)

        scrap.Execute(sourcename)

    def saved_handler(self, sender, **kwargs):
        print(sender)