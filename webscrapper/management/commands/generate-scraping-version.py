"""
    __author__     = "Moisés Rangel"
    __license__    = "GPL"
    __version__    = "0.5"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""

from decimal import Decimal
from django.core.management.base  import BaseCommand, CommandError
from webscrapper.models           import Version

class Command(BaseCommand):
    help = 'Scraping version'

    def handle(self, *args, **options):
        items          = Version.objects.all().order_by('-created_date')

        last_version   = Decimal('0.0')
        if len(items) > 0:
            last_version   = items[0].number

        new_version    = last_version + Decimal('0.10')

        version = Version()
        version.number = new_version
        version.save()

        self.stdout.write(self.style.SUCCESS('Successfully created version "%s"' % version.number))




