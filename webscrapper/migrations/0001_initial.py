# Generated by Django 2.2.2 on 2019-06-29 18:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('url', models.CharField(max_length=200)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('number', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('address', models.CharField(max_length=200)),
                ('baths', models.DecimalField(decimal_places=1, max_digits=3)),
                ('colony', models.CharField(max_length=200)),
                ('colony_slug', models.CharField(max_length=200)),
                ('delivery_date', models.DateField()),
                ('description', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('propertytype', models.CharField(choices=[('C', 'Casa'), ('D', 'Departamento'), ('T', 'Terreno'), ('Local', 'Local')], max_length=200)),
                ('rooms', models.IntegerField()),
                ('source', models.CharField(choices=[('ML', 'Mercado Libre'), ('SM', 'Segundamano'), ('M3', 'Metros Cubicos')], max_length=200)),
                ('state', models.CharField(max_length=200)),
                ('state_slug', models.CharField(max_length=200)),
                ('title', models.CharField(max_length=200)),
                ('town', models.CharField(max_length=200)),
                ('town_slug', models.CharField(max_length=200)),
                ('url', models.URLField()),
                ('photos', models.ManyToManyField(to='webscrapper.Photo')),
                ('version', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webscrapper.Version')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
