from django.contrib import admin
from webscrapper.models import Property, Version

class PropertyAdmin(admin.ModelAdmin):
	list_filter = ['version', 'source', 'operationtype', 'propertytype']

admin.site.register(Property, PropertyAdmin)
admin.site.register(Version)

