# Real Estate - Webscraping

Proyecto de consola para extraer datos de diferentes sitios de venta/renta de inmuebles.

### Prerequisitos

El proyecto hace uso de Django, BeautifulSoup y requests, solo instala las dependencias

```
pip install -r requirements.txt
```

### Instalación

La aplicación hace uso de PostgreSQL, por lo cuál hay que configurar una base de datos y un usuario dentro de PostgreSQL y cambiar la configuración dentro de **realstate/settings.py** en el diccionario de **DATABASES**.

Una vez creada la base de datos, crea y ejecuta las migraciones de la bd.

```
python manage.py makemigrations
```

y

```
python manage.py migrate
```

Crea un superusuario para poder acceder al admin de Django

```
python manage.py createsuperuser
```

### Configurar folder de STATIC_PATH_PHOTOS

Especifica una ruta válida y con permisos de escritura para la variable STATIC_PATH_PHOTOS dentro de settings.py


### Generar una versión para la extración de datos.

```
python manage.py generate-scraping-version
```

![picture](images/version.jpg)

### Extraer datos

Por último solo ejecuta el modulo a extraer (python manage.py scraping [modulo]):

```
python manage.py scraping MercadoLibre
```

![picture](images/scraping.jpg)

### Visualizar datos

Puedes activar el servidor de desarrollo de Django para visualizar los datos extraídos.

```
python manage.py runserver
```

Y acceder en tu navegador a : http://localhost:8000/admin


![picture](images/admin.jpg)

### Desarrollado con

* [Django](https://www.djangoproject.com/)
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)


### Autores

* **Moisés Rangel**


## Licencia

This project is licensed under the GPL License - see the [LICENSE.md](https://www.gnu.org/licenses/licenses.es.html) file for details

